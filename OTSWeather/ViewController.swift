//
//  ViewController.swift
//  OTSWeather
//
//  Created by Алексей Смоляк on 11/30/19.
//  Copyright © 2019 Алексей Смоляк. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UISearchResultsUpdating {
    //MARK: - Properties
    var timer = Timer()
    
    //MARK: - LifeCurcle
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Hello World!")
        
        self.setupNavigationBar()
    }
    fileprivate func setupNavigationBar() {
        self.navigationItem.title = "Weather application"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        let searchControlelr = UISearchController(searchResultsController: nil)
        searchControlelr.searchResultsUpdater = self
        navigationItem.searchController = searchControlelr
        navigationItem.hidesSearchBarWhenScrolling = false
    }
    
    //MARK: - UpdateSearchResults
    func updateSearchResults(for searchController: UISearchController) {
        let city = searchController.searchBar.text!
        timer.invalidate()
        
        if city != "" {
            timer = Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false, block: { (timer) in
                ApiManager.shared.getWeather(city: city) { (model) in
                    for list in model!.list! {
                        print(list.main?.temp)
                    }
                }
            })
        }
        
    }
}

