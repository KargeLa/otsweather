//
//  ApiManager.swift
//  OTSWeather
//
//  Created by Алексей Смоляк on 12/2/19.
//  Copyright © 2019 Алексей Смоляк. All rights reserved.
//

import Foundation

class ApiManager {
    private init() {}
    
    static let shared:ApiManager = ApiManager()
    
    func getWeather (city: String, result: @escaping ((WeatherInfo?) -> ())) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "samples.openweathermap.org"
        urlComponents.path = "/data/2.5/forecast"
        urlComponents.queryItems = [URLQueryItem(name: "q", value: city),
                                    URLQueryItem(name: "appid", value: "f978de9045bf353948e23cb511f2ce42")]
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = "GET"
        
        let task = URLSession(configuration: .default)
        task.dataTask(with: request) { (data, response, error) in
            if error == nil {
                let decoder = JSONDecoder()
                var decoderWeatherInfo: WeatherInfo?
                
                if data != nil {
                    decoderWeatherInfo = try? decoder.decode(WeatherInfo.self, from: data!)
                }
                
                result(decoderWeatherInfo)
            } else {
                print(error as Any)
            }
        }.resume()
    }
}

