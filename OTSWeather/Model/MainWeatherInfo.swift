//
//  MainWeatherInfo.swift
//  OTSWeather
//
//  Created by Алексей Смоляк on 12/2/19.
//  Copyright © 2019 Алексей Смоляк. All rights reserved.
//

import Foundation

class MainWeatherInfo: Codable {
    var temp: Float?
}
