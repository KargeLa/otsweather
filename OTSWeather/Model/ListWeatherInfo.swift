//
//  ListWeatherInfo.swift
//  OTSWeather
//
//  Created by Алексей Смоляк on 12/2/19.
//  Copyright © 2019 Алексей Смоляк. All rights reserved.
//

import Foundation

class  ListWeatherInfo: Codable {
    var dt: Float?
    var main: MainWeatherInfo?
}
